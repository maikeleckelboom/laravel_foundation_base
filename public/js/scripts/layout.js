  // Enable Javascript & Jquery
$(document).foundation();
// Enable Ajax calls
$(document).ajaxComplete(function () {
    $(document).foundation();
});

$(document).ready(function () {
    $('#offCanvasMain').on('opened.zf.offcanvas', function () {
        $('body').addClass('menu-open');
    });

    $('#offCanvasMain').on('closed.zf.offcanvas', function () {
        $('body').removeClass('menu-open');
    });
});

// $(document).ready(function() {
// //     $(".canvas-logo-content").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)");
// // });


// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('.header').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('.header').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('.header').removeClass('nav-up').addClass('nav-down');
        }
    }

    lastScrollTop = st;
}

// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('header, .off-canvas-content').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('header, .off-canvas-content').removeClass('nav-up').addClass('nav-down');
        }
    }

    lastScrollTop = st;
}


$(document).on('open.zf.reveal', "#modal", function (e) {
    var $modal = $(this);
    var ajax_url = $modal.data("ajax-url");
    if (ajax_url) {
        $modal.html("<img src=\"files/Spinner-1s-200px.svg')\">");
        $.ajax(ajax_url).done(function (response) {
            $modal.html(response);
        });
    }
});


$(document).on('open.zf.reveal', "#profile-menu-modal", function (e) {
  var $modal = $(this);
  var ajax_url = $modal.data("ajax-url");
  if (ajax_url) {
      // $modal.html("Now Loading: "+ajax_url);
      $modal.html("<img src=\"files/Spinner-1s-200px.svg')\">");

      $('#profile-menu-modal').trigger('resizeme.zf.reveal');
      $.ajax(ajax_url).done(function (response) {
          $modal.html(response);
      });
  }
});


jQuery(function($) {
$(document).ajaxStart(function() {
    if ($('#loading-bar').length === 0) {
        $('body').append( $('<div/>').attr('id', 'loading-bar').addClass(_lb.position) );
        _lb.percentage = Math.random() * 30 + 30;
        $("#loading-bar")[_lb.direction](_lb.percentage + "%");
        _lb.interval = setInterval(function() {
            _lb.percentage = Math.random() * ( (100-_lb.percentage) / 2 ) + _lb.percentage;
            $("#loading-bar")[_lb.direction](_lb.percentage + "%");
        }, 500);
    }
    }).ajaxStop(function() {
        clearInterval(_lb.interval);
        $("#loading-bar")[_lb.direction]("101%");

        /**Waits until css transition is finished and removes element from the DOM*/
        setTimeout( function() {
            $("#loading-bar").fadeOut(300, function() {
                $(this).remove();
            });
        }, 300);

    });
});

/*** Main object*/
var _lb = {};

//Default loading bar position
_lb.position  = 'top';
_lb.direction = 'width';

/*** Ajax call
 * accepts callback( response )*/
_lb.get = function( callback ) {
    _lb.loading = true;
    jQuery.ajax({
        url   : this.href,
        success: function(response) {
            _lb.loading = false;
            if ( typeof(callback) === 'function'  )
                callback( response );
        }
    });
};

jQuery(function($) {
    $('.btn-action').click(function() {
        switch ( $(this).data('action') ) {
            case 'load':
                _lb.get();
                break;
            case 'position':
                _lb.position  = $(this).data('position');
                _lb.direction = $(this).data('direction');
                $('#section-position h1 small').html( _lb.position );
                break;
        }
    });
});
  //
  // $(function() {
  //     //The passed argument has to be at least a empty object or a object with your desired options
  //     $('textarea').overlayScrollbars({
  //         className       : "os-theme-minimal-dark",
  //         resize          : false,
  //         sizeAutoCapable : false,
  //         // paddingAbsolute : true,
  //         scrollbars : {
  //             clickScrolling : true
  //         }
  //     });
  // });

  $('#reveal1').on('open.zf.reveal', function() {
      //artificially slow down load to let you see the loading state
      setTimeout(function() {
          $.ajax('https://codepen.io/kball/pen/rLKrkO.html').
          done(function(content) {
              $('#reveal1').html(content);
              $('#reveal1').trigger('resizeme.zf.reveal');
          });
      }, 500);
  });

  $(function() {
      //The passed argument has to be at least a empty object or a object with your desired options
      $('.position-left').overlayScrollbars({
          className       : "os-theme-minimal-dark",
          resize          : false,
          sizeAutoCapable : false,
          // paddingAbsolute : true,
          scrollbars : {
              clickScrolling : true
          }
      });
  });

  $(function() {
      //The passed argument has to be at least a empty object or a object with your desired options
      $('#select2-category_id-q1-results').overlayScrollbars({

      });
  });


  $(document).foundation();

  $('#trigger-reveal').on('click', function() {
      $.ajax('https://codepen.io/rafibomb/pen/XKEgmL.html').
      done(function(content) {
          $('#reveal1').html(content).foundation('open');
      });
  });



  $(document).ready(function() {
      var bigimage = $(".product-slider");
      var thumbs = $("#thumbs");
      //var totalslides = 10;
      var syncedSecondary = true;

      bigimage
          .owlCarousel({
              items: 1,
              slideSpeed: 4000,
              nav: true,
              autoplay: true,
              dots: false,
              loop: false,
              stadePadding: false,
              responsiveRefreshRate: 200,
              navText: [
                  '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
                  '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
              ]
          })
          .on("changed.owl.carousel", syncPosition);

      thumbs
          .on("initialized.owl.carousel", function() {
              thumbs
                  .find(".owl-item")
                  .eq(0)
                  .addClass("current");
          })
          .owlCarousel({
              items: 4,
              dots: true,
              nav: true,
              navText: [
                  '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
                  '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
              ],
              smartSpeed: 200,
              slideSpeed: 500,
              slideBy: 4,
              responsiveRefreshRate: 100
          })
          .on("changed.owl.carousel", syncPosition2);

      function syncPosition(el) {
          //if loop is set to false, then you have to uncomment the next line
          //var current = el.item.index;

          //to disable loop, comment this block
          var count = el.item.count - 1;
          var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

          if (current < 0) {
              current = count;
          }
          if (current > count) {
              current = 0;
          }
          //to this
          thumbs
              .find(".owl-item")
              .removeClass("current")
              .eq(current)
              .addClass("current");
          var onscreen = thumbs.find(".owl-item.active").length - 1;
          var start = thumbs
              .find(".owl-item.active")
              .first()
              .index();
          var end = thumbs
              .find(".owl-item.active")
              .last()
              .index();

          if (current > end) {
              thumbs.data("owl.carousel").to(current, 100, true);
          }
          if (current < start) {
              thumbs.data("owl.carousel").to(current - onscreen, 100, true);
          }
      }

      function syncPosition2(el) {
          if (syncedSecondary) {
              var number = el.item.index;
              bigimage.data("owl.carousel").to(number, 100, true);
          }
      }

      thumbs.on("click", ".owl-item", function(e) {
          e.preventDefault();
          var number = $(this).index();
          bigimage.data("owl.carousel").to(number, 300, true);
      });
  });
