$('.home-users-carousel').owlCarousel({
    loop: false,
    margin: 10,
    dots: false,
    nav: true,
    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    autoplay: false,
    autoplayHoverPause: true,
    stagePadding: 24,
    responsive: {
        0: {
            items: 4
        },
        479: {
            items: 4
        },
        639: {
            items: 4
        },
        767: {
            items: 5
        },
        1023: {},
        1199: {
            items: 6
        }
    }
});

$('.home-posts-carousel').owlCarousel({
    loop: false,
    dots: false,
    nav: true,
    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    autoplay: false,
    autoplayHoverPause: true,
    margin: 10,
    responsive: {
        0: {
            items: 1,
            stagePadding: 24
        },
        479: {
            items: 1,
            stagePadding: 24
        },
        639: {
            items: 1,
            stagePadding: 0
        },
        767: {
            items: 2,
            stagePadding: 0
        },
        1023: {
            items: 2,
            stagePadding: 0
        },
        1199: {
            items: 3,
            stagePadding: 0
        }
    }
});

$('.home-tags-carousel').owlCarousel({
    loop: false,
    dots: false,
    nav: true,
    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    autoplay: false,
    autoplayHoverPause: true,
    stagePadding: 24,
    responsive: {
        0: {
            items: 5
        },
        479: {
            items: 6
        },
        639: {
            items: 8
        },
        767: {
            items: 10
        },
        1023: {},
        1199: {
            items: 14
        }
    }
});

$('.home-categories-carousel').owlCarousel({
    loop: false,
    dots: false,
    nav: true,
    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    autoplay: false,
    autoplayHoverPause: true,
    stagePadding: 24,
    responsive: {
        0: {
            items: 3
        },
        479: {
            items: 3
        },
        639: {
            items: 4
        },
        767: {
            items: 6
        },
        1023: {},
        1199: {
            items: 6
        }
    }
});

$('.tags-small').owlCarousel({
    loop: false,
    dots: false,
    nav: false,
    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    autoplay: false,
    autoplayHoverPause: true,
    stagePadding: 0,
    responsive: {
        0: {
            items: 3
        },
        479: {
            items: 3
        },
        639: {
            items: 4
        },
        767: {
            items: 6
        },
        1023: {},
        1199: {
            items: 6
        }
    }
});

