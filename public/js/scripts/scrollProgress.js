// Scroll progress Visialiazation
$(document).ready(function () {

    var getMax = function () {
        return $(document).height() - $(window).height();
    };

    var getValue = function () {
        return $(window).scrollTop();
    };

    if ('max' in document.createElement('progress')) {
        let progressBar = $('progress');

        progressBar.attr({
            max: getMax()
        });

        $(document).on('scroll', function () {
            progressBar.attr({
                value: getValue()
            });
        });

        $(window).resize(function () {

            progressBar.attr({
                max: getMax(),
                value: getValue()
            });
        });

    } else {

        let progressBar = $('.progress-bar'),
            max = getMax(),
            value, width;

        let getWidth = function () {

            value = getValue();
            width = (value / max) * 100;
            width = width + '%';
            return width;
        };

        let setWidth = function () {
            progressBar.css({
                width: getWidth()
            });
        };

        $(document).on('scroll', setWidth);
        $(window).on('resize', function () {

            max = getMax();
            setWidth();
        });
    }
});
