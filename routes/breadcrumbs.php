<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});


// Home > Blog
Breadcrumbs::for('blog', function ($trail) {
    $trail->parent('home');
    $trail->push('Blog', route('blog.index'));
});


// Home > Blog > Single
Breadcrumbs::for('single', function ($trail) {
    $trail->parent('blog');
    $trail->push('Post', route('categories.index'));
});


// Home > Blog > Create
Breadcrumbs::for('create', function ($trail) {
    $trail->parent('blog');
    $trail->push('Create', route('posts.create'));
});


// Home > Blog > Posts
Breadcrumbs::for('posts', function ($trail) {
    $trail->parent('blog');
    $trail->push('Posts', route('posts.index'));
});

// Home > Blog > Posts
Breadcrumbs::for('blog_single', function ($trail, $post) {
    $trail->parent('blog');
    $trail->push($post->slug, route('posts.show', $post->slug));
});


// Home > Blog > Categories
Breadcrumbs::for('categories', function ($trail) {
    $trail->parent('blog');
    $trail->push('Categories', route('categories.index'));
});


// Home > Blog > Tags
Breadcrumbs::for('tags', function ($trail) {
    $trail->parent('blog');
    $trail->push('Tags', route('tags.index'));
});

// Home > Blog > [Category] > [Post]
Breadcrumbs::for('post_category', function ($trail, $post) {
    $trail->parent('category', $post->category);
    $trail->push($post->title, route('post', $post->id));
});


// Home > Blog > [Category] > [Post]
Breadcrumbs::for('post', function ($trail, $post) {
    $trail->parent('category', $post->category);
    $trail->push($post->title, route('post', $post->id));
});
