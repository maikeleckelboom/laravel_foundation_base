<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', 'HomeController@index')->name('home');


/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/

Auth::routes();

Route::get('profile', 'UserProfileController@show')->middleware('auth')->name('profile.show');
Route::post('profile', 'UserProfileController@update_avatar')->middleware('auth')->name('profile.update');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


/*
|--------------------------------------------------------------------------
| Blog Routes
|--------------------------------------------------------------------------
*/

//  Index
Route::get('blog', 'Blog\BlogController@index')->name('blog.index');

//  Single
Route::get('blog/{slug}', 'Blog\BlogController@single')->where('slug', '[\w\d\-\_]+')->name('blog.single');


/*
 * | Posts
|--------------------------------------------------------------------------
*/

//  Resource
Route::resource('posts', 'Blog\PostController');


//  Index
//Route::get('blog/posts', 'Blog\PostController@index')->name('blog.posts.index');

//  Create
Route::get('create', 'Blog\PostController@create');

//  Store
Route::get('store', function () {
    return view('blog.posts.store');
});

//  Show
Route::get('show', function () {
    return view('blog.posts.show');
});


//

Route::get('webshop', function () {
    return view('webshop.index');
});


/*
 * | Comments
|--------------------------------------------------------------------------
*/

Route::post('comments/{post_id}', ['uses' => 'Blog\CommentsController@store', 'as' =>'comments.store']);
Route::get('comments/{id}/edit', ['uses' => 'Blog\CommentsController@edit', 'as' => 'comments.edit']);
Route::put('comments/{id}', ['uses' => 'Blog\CommentsController@update', 'as' => 'comments.update']);
Route::get('comments/{id}/delete', ['uses' => 'Blog\CommentsController@delete', 'as' => 'comments.delete']);
Route::delete('comments/{id}', ['uses' => 'Blog\CommentsController@destroy', 'as' => 'comments.destroy']);



/*
 * | Categories
|--------------------------------------------------------------------------
*/

Route::resource('categories', 'Blog\CategoryController', ['except' => ['create']]);



/*
 * | Tags
|--------------------------------------------------------------------------
*/

Route::get('tags/find', 'Blog\TagController@find');
Route::resource('tags', 'Blog\TagController', array('except' => array('create', 'find')));

Route::get('pages/check_slug', 'SlugController@check')->name('pages.check_slug');



// Ajax Routes
Route::get('entry', function () {
    return view('auth.entry');
});

Route::get('profile-menu', function(){
    return view('partials.menu.profile-menu');
});
