<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
//use Sofa\Eloquence\Eloquence;
//
//class Tag extends Model
//{
//
//    public function posts()
//    {
//        return $this->belongsToMany('App\Post');
////        return $this->hasMany(Post::class); // also Post belongsToMany Categories
//
//    }
//}

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Tag extends Model
{
    use Eloquence;

    protected $searchableColumns = ['name'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

        public function posts()
    {
        return $this->belongsToMany('App\Post');

    }
}
