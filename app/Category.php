<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // Use the categories table when working with this model
    protected $table = 'categories';

    // Create a method
    public function Posts() {
        return $this->hasMany('App\Post');
    }
}
