<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Slug;

class SlugController extends Controller
{
    public function check(Request $request)
    {
        $slug = SlugService::createSlug(Slug::class, 'slug', $request->title);

        return response()->json(['slug' => $slug]);
    }

}
