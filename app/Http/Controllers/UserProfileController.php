<?php

namespace App\Http\Controllers;

use App\Category;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Image;


class UserProfileController extends Controller
{
    public function show(){
        $categories = Category::all();
        $tags = Tag::all();

        return view('profile', array('user' => Auth::user()) , compact('tags', 'categories'));
    }

    public function update_avatar(Request $request){

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/storage/users/avatars/' . $filename ) );
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }

        return view('profile', ['user' => Auth::user()] );
    }
}
