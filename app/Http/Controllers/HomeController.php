<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use http\Client\Curl\User;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $tags = Tag::all();
        $users = \App\User::all();
        $posts = Post::orderBy('created_at', 'desc')->limit(14)->get();

        return view('home', compact('posts', 'users', 'categories', 'tags'));
    }


}
