<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Mews\Purifier\Purifier;
use App\User;
use App\Post;
use App\Category;
use App\Tag;
use App\Slug;
use Image;
use Session;
use Mtownsend\ReadTime\ReadTime;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function check_slug(Request $request)
    {
        // New version: to generate unique slugs
        $slug = SlugService::createSlug(Post::class, 'slug', $request->title);

        return response()->json(['slug' => $slug]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::orderBy('id', 'desc') -> paginate(5);
        $categories = Category::all();
        $tags = Tag::all();

        return view('blog.posts.index', compact('posts', 'categories', 'tags'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        $categories = Category::all();

        return view('blog.posts.create')->withCategories($categories)->withTags($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // validate the data
        $this->validate($request, array(
            'title' => 'required|max:255',
            'category_id' => 'required|integer',
            'slug'  => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
            'body'  => 'required',
            'featured_image' => 'sometimes|image'
        ));

        // Store in database
        $post = new Post;
        $post->user_id = auth()->id();
        $post->category_id = $request->category_id;
        $post->Title = $request->title;
        $post->slug = $request->slug;
        $post->body = (clean($request->body));

        if ($request->hasFile('featured_image')) {
            $image = $request->file('featured_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('storage/blog/images/' . $filename);
            Image::make($image)->resize(800, 400)->save($location);

            $post->image = $filename;
        }



        $post->save();

        $post->tags()->sync($request->tags, false);

        Session::flash('success', 'Data Successfully inserted');

        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $user = User::find($id);
        $categories = Category::all();
        $tags = Tag::all();


        return view('blog.posts.show', compact('post', 'user', 'categories', 'tags'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $categories = Category::all();
        $tags = Tag::all();

        $tagsArray = array();
        foreach($tags as $tag) {
            $tagsArray[$tag->id] = $tag->name;
        }

        return view('blog.posts.edit', compact('post', 'categories', 'tags'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate the data
        $post = Post::find($id);

        $this->validate($request, array(
            'title' => 'required|max:255',
            'slug'  => "required|alpha_dash|min:5|max:255|unique:posts,slug,$id",
            'body'  => 'required',
            'featured_image' => 'image'
        ));


        // Save the data to the database
        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->slug = $request->input('slug');
        $post->category_id = $request->input('category_id');
        $post->body = (clean($request->input('body')));


        if ($request->hasFile('featured_image')) {

            // add the new photo
            $image = $request->file('featured_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('storage/blog/images/' . $filename);
            Image::make($image)->resize(800, 400)->save($location);

            // get filename
            $oldFilename = $post->image;

            // update the new photo
            $post->image = $filename;

            // delete the old photo
            Storage::delete($oldFilename);

        }

        $post->save();

        $post->tags()->sync($request->tags, true);

        // set flash data with success message
        Session::flash('success', 'Data Successfully updated');

        // redirect with flash data to posts.show
        return redirect()->route('posts.show', $post->id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find the post in the database
        $post = Post::find($id);
        $post->tags()->detach();
        Storage::delete($post->image);

        // Delete the post
        $post->delete();

        // Flash data with success message
        Session::flash('success', 'Data destroyed');

        // redirect with flash data to posts.show
        return redirect()->route('posts.index');
    }
}
