<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Category;
use App\Tag;
use App\Post;
use App\User;


class BlogController extends Controller
{

    public function hasCommentsFromUser($userId){
        return $this->comments()->where('user_id', $userId)->count() > 0;
    }

    public function index() {

        // Fetch all posts in desc order and paginate
        $posts = Post::latest('created_at')->paginate(48);
        $users = User::all();
        $categories = Category::all();
        $tags = Tag::all();

        // return the view and pass in the post object
        return view('blog.index', compact('posts', 'users', 'tags', 'categories'));
    }


    public function single($slug) {

        // fetch from the DB based on slug
        $post = Post::where('slug', '=', $slug)->first();
        $user = User::all();
        $categories = Category::all();
        $tags = Tag::all();

        // return the view and pass in the post object
        return view('blog.single', compact('post', 'user', 'tags', 'categories'));

    }

//    /**
//     * Returns an estimated reading time in a string
//     * idea from @link http://briancray.com/posts/estimated-reading-time-web-design/
//     * @param  string $content the content to be read
//     * @return string          estimated read time eg. 1 minute, 30 seconds
//     */
//    function estimate_reading_time($content) {
//        $word_count = str_word_count(strip_tags($content));
//
//        $minutes = floor($word_count / 200);
//        $seconds = floor($word_count % 200 / (200 / 60));
//
//        $str_minutes = ($minutes == 1) ? "minute" : "minutes";
//        $str_seconds = ($seconds == 1) ? "second" : "seconds";
//
//        if ($minutes == 0) {
//            return "{$seconds} {$str_seconds}";
//        }
//        else {
//            return "{$minutes} {$str_minutes}, {$seconds} {$str_seconds}";
//        }
//    }

}
