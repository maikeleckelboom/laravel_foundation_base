<?php


namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;


class UserController extends Controller
{
    public function Show(){
        $users = User::latest('created_at');
        $categories = Category::all();
        $tags = Tag::all();

        // Return the view
        return view('home', compact('users', 'categories', 'tags'));
    }


}