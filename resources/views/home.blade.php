@extends('layouts.app')

@section('title', 'Home')

@section('stylesheets')
    <link rel="stylesheet" href="css/owlCarousel/owl.carousel.css">
@endsection

@section('scripts')
    <script src="{{ asset('js/owlCarousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('js/scripts/owl-customs.js') }}"></script>
@endsection

@section('classes')
    class="home"
@endsection

@section('content')
    <div class="grid-container">
        <div class="grid-x">

            <div class="cell small-24 collapsable">
                <div class="grid-x grid-block">
                    <!-- Top left -->
                    <div class="cell top-left-22">
                        <div class="breadcrumbs-bar">
                            {{ Breadcrumbs::render('home') }}
                        </div>
                    </div>
                    <!-- Top right -->
                    <div class="cell top-right-2">
                    </div>
                </div>
            </div>


            <div class="cell small-24 section">
                <div class="owl-carousel home-users-carousel">
                    @foreach($users as $user)
                        <div class="item">
                            <div class="user-container">
                                <img src="/storage/users/avatars/{{ $user->avatar }}" class="user-avatar"
                                     id="user-dots-menu" data-toggler="user-dots-menu">
                            </div>

                            <div class="user-data-group">
                                <div class="grid-x">
                                    <div class="cell small-22">
                                        <a href="#" class="data-user">{{ $user->name }}</a>
                                    </div>
                                    <div class="small-2">
                                        <a href="#">
                                            <img src="files/dots_button.svg">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>


{{--            <div class="cell small-24 section">--}}
{{--                <div class="owl-carousel home-posts-carousel">--}}
{{--                    @foreach($posts as $post)--}}
{{--                        <div class="item">--}}

{{--                            <div class="cell image-unit">--}}
{{--                                @if( !empty($post->image))--}}
{{--                                    <img src="{{ asset('storage/blog/images') . '/' . $post->image }}">--}}
{{--                                @else--}}
{{--                                    <img src="{{ asset('storage/blog/images') . '/' . 'default.jpg' }}">--}}
{{--                                @endif--}}
{{--                            </div>--}}

{{--                            <div class="cell title-unit">--}}
{{--                                <span class="lead">{{ $post->title }}</span>--}}
{{--                            </div>--}}

{{--                            <div class="cell body-unit">--}}
{{--                                <p>{{ substr(strip_tags($post->body), 0, 78) }}{{ strlen(strip_tags($post->body)) > 80 ? "..." : "" }}</p>--}}
{{--                            </div>--}}

{{--                            <div class="cell category-unit">--}}
{{--                                <p>{{ $post->category['name'] }}</p>--}}
{{--                            </div>--}}

{{--                            <div class="cell user-unit">--}}
{{--                                <img src="/storage/users/avatars/{{ $post->user['avatar'] }}" class="post-user-avatar">--}}
{{--                                <span>{{ $post->user['name'] }}</span>--}}
{{--                            </div>--}}

{{--                            <p>{{ $post->created_at->format('F d, Y') }}</p>--}}

{{--                            <div class="tag-label-container">--}}
{{--                                @foreach ($post->tags as $tag)--}}
{{--                                    <a href="{{route('tags.show', $tag->id)}}">--}}
{{--                                        <span class="tag-shape">{{ $tag->name }}</span>--}}
{{--                                    </a>--}}
{{--                                @endforeach--}}
{{--                            </div>--}}

{{--                            <a href="{{ url('blog/'.$post->slug) }}" class="read-more-button">--}}
{{--                                @include('partials.svg.angle-right')--}}
{{--                                {{__('Lees meer') }}--}}
{{--                            </a>--}}

{{--                            <span>{{ read_time($post->body) }}</span>--}}

{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="cell small-24 section">--}}
{{--                @foreach($categories as $category)--}}
{{--                    <a href="{{ route('categories.show', $category->id) }}">--}}
{{--                        <span class="category-shape">{{ $category->name }}</span>--}}
{{--                    </a>--}}
{{--                @endforeach--}}
{{--            </div>--}}


{{--            <div class="cell small-24 section">--}}
{{--                @foreach($tags as $tag)--}}
{{--                    <a href="{{ route('tags.show', $tag->id) }}">--}}
{{--                        <span class="tag-shape">{{ $tag->name }}</span>--}}
{{--                    </a>--}}
{{--                @endforeach--}}
{{--            </div>--}}



        </div>
    </div>
@endsection
