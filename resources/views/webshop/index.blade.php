@extends('layouts.app')

@section('title', 'Home')

@section('stylesheets')
    <link rel="stylesheet" href="css/owlCarousel/owl.carousel.css">
@endsection

@section('scripts')
    <script src="{{ asset('js/owlCarousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('js/scripts/owl-customs.js') }}"></script>
@endsection

@section('classes')
    class="webshop"
@endsection

@section('content')
    <div class="grid-container">
        <div class="grid-x">

            <div class="cell small-24 collapsable">
                <div class="grid-x grid-block">
                    <!-- Top left -->
                    <div class="cell top-left-22">
                        <div class="breadcrumbs-bar">
                            {{ Breadcrumbs::render('home') }}
                        </div>
                    </div>
                    <!-- Top right -->
                    <div class="cell top-right-2">
                    </div>
                </div>
            </div>


            <div class="outer">
                <div id="big" class="owl-carousel owl-theme product-slider">
{{--                    <div class="item">--}}
{{--                        <h1><img src="{{ asset('files/banners/banner_1.jpg') }}"></h1>--}}
{{--                    </div>--}}
{{--                    <div class="item">--}}
{{--                        <h1><img src="{{ asset('files/banners/banner_2.jpg') }}"></h1>--}}
{{--                    </div>--}}
{{--                    <div class="item">--}}
{{--                        <h1><img src="{{ asset('files/banners/banner_3.jfif') }}"></h1>--}}
{{--                    </div>--}}
{{--                    <div class="item">--}}
{{--                        <h1><img src="{{ asset('files/banners/banner_4.jpg') }}"></h1>--}}
{{--                    </div>--}}
                    <div class="item">
                        <h1>1</h1>
                    </div>
                    <div class="item">
                        <h1>2</h1>
                    </div>
                    <div class="item">
                        <h1>3</h1>
                    </div>
                    <div class="item">
                        <h1>4</h1>
                    </div>
                    <div class="item">
                        <h1>5</h1>
                    </div>
                    <div class="item">
                        <h1>6</h1>
                    </div>

                </div>
                <div id="thumbs" class="owl-carousel owl-theme">
{{--                    <div class="item">--}}
{{--                        <h1><img src="{{ asset('files/banners/banner_1.jpg') }}"></h1>--}}
{{--                    </div>--}}
{{--                    <div class="item">--}}
{{--                        <h1><img src="{{ asset('files/banners/banner_2.jpg') }}"></h1>--}}
{{--                    </div>--}}
{{--                    <div class="item">--}}
{{--                        <h1><img src="{{ asset('files/banners/banner_3.jfif') }}"></h1>--}}
{{--                    </div>--}}
{{--                    <div class="item">--}}
{{--                        <h1><img src="{{ asset('files/banners/banner_4.jpg') }}"></h1>--}}
{{--                    </div>--}}
                    <div class="item">
                        <h1>1</h1>
                    </div>
                    <div class="item">
                        <h1>2</h1>
                    </div>
                    <div class="item">
                        <h1>3</h1>
                    </div>
                    <div class="item">
                        <h1>4</h1>
                    </div>
                    <div class="item">
                        <h1>5</h1>
                    </div>
                    <div class="item">
                        <h1>6</h1>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection

@section('scripts')
    <script>

    </script>
@stop
