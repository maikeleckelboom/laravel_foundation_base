@extends('layouts.app')

<?php $titleTag = htmlspecialchars($post->title) ?>
@section('title', "$titleTag")

@section('stylesheets')
    {{--    JS Exception   --}}
    <script
        src="https://cdn.tiny.cloud/1/uw677gk2zbccsroh2c26ut5na84yf75kge04kxeluvybpk3g/tinymce/5/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'link, image, imagetools'
        })
    </script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/parsely.css') }}">
@endsection

@section('classes')
    class="scroll-progress"
@endsection

@section('content')
    <div class="grid-x grid-margin-x grid-margin-y grid-padding-x">

        <div class="cell small-24">
            <div class="grid-x grid-block">
                <!-- Top left -->
                <div class="cell top-left-22">
                    <div class="breadcrumbs-bar">
                        <a href="{{ url()->previous() }}" class="go-back">
                            <img src="{{ asset('files/line-angle-left.svg') }}">
                            <span>{{__('Back')}}</span>
                        </a>
                        {{ Breadcrumbs::render('blog_single', $post) }}
                    </div>
                </div>
                <!-- Top right -->
                <div class="cell top-right-2">
                    <div class="component-button">
                        <button type="button" class="button admin-panel" data-toggle="offCanvasRight">
                            {{__('Admin Panel') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="cell small-24 medium-18">
            <div class="blog-post-container">
                <div class="blog-image">
                    @if( !empty($post->image))
                        <img src="{{ asset('storage/blog/images') . '/' . $post->image }}">
                    @else
                        <img src="{{ asset('storage/blog/images') . '/' . 'default.jpg' }}">
                    @endif
                </div>
                <div class="blog-post">
                    <span class="heading h3">{{ $post->title }}</span>
                    <span class="lead">{!! $post->body !!} </span>
                </div>
            </div>
        </div>

        <div class="cell small-24 medium-6">
            <ul class="no-bullet">
                <li>
                    <img src="/storage/users/avatars/{{ $post->user['avatar'] }}" class="post-user-avatar">
                    {{ $post->user['name'] }}
                </li>
                <li>
                    <div class="">
                        <a href="{{ route('categories.show', $post->category) }}" class="category-shape text-center" id="category">
                            {{ $post->category['name']  }}
                        </a>
                    </div>
                </li>
                <li>
                    @foreach ($post->tags as $tag)
                        <a href="{{route('tags.show', $tag->id)}}">
                            <span class="tag-shape">
                                <span class="tag-after">{{ $tag->name }}</span>
                            </span>
                        </a>
                    @endforeach
                </li>
                <li class="est-read">
                    <img src="{{ asset('files/icons/Hourglass-80_icon-icons.com_57316.svg') }}">
                    <span>{{ read_time($post->body) }}</span>
                </li>
            </ul>
        </div>


        <div class="cell small-24 medium-18 comments-block">
            <div class="check-for-comments">
                @if($post->comments()->count())
                    <div class="comment-count">
                        <img src="{{ asset('files/icons/comments-icon.png') }}">
                        <span data-tooltip
                              aria-haspopup="true"
                              class="has-tip top counter"
                              data-click-open="true"
                              data-disable-hover="false"
                              tabindex="2"
                              title="Comment Counter">
                            <span class="count-text">{{ $post->comments()->count() }}</span>
                        </span>
                    </div>
                    <div>
                        @foreach($post->comments as $comment)
                            <div class="grid-x grid-margin-y section comment-item">

                                <div class="cell small-6 author-info">
                                    <div class="grid-x author-block">
                                        <div class="cell small-24">
                                            <img
                                                src="{{ "https://www.gravatar.com/avatar/" . md5(strtolower(trim($comment->email))) . "?s=50&d=retro" }}"
                                                class="author-image">
                                            <span class="lead">{{ $comment->name }}</span>
                                        </div>
                                        <div class="cell small-24">
                                            <p class="author-time">{{ date('F nS, Y - g:iA' ,strtotime($comment->created_at)) }}</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="cell small-18 comment-content">
                                    {!! $comment->comment !!}
                                </div>

                            </div>
                        @endforeach
                    </div>
                    <button class="start-conversation">
                        {{__('Click here to continue the conversation!') }}
                    </button>
                @else
                    <button class="start-conversation">
                        {{__('Click here to start the conversation!') }}
                    </button>
                @endif
            </div>

            <div class="section comment-form" id="comment-form">
                <form method="POST" action="{{ route('comments.store', $post->id) }}" data-parsley-validate>

                    <div class="grid-x grid-margin-x">
                        <div class="cell small-12">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Naam" required>
                        </div>

                        <div class="cell small-12">
                            <input type="email" id="email" name="email" placeholder="Email adress" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <textarea id="comment" name="comment" rows="10" required></textarea>
                    </div>

                    <div class="component-button">
                        <button type="submit" value="Reageer op dit bericht" class="button comment-submit">Submit
                        </button>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                    </div>

                </form>
            </div>
        </div>


        <hr>

    </div>
@endsection

{{--    Admin Panels    --}}
@section('panel-col-2')
    <div class="cell small-24">
        <span>Slug</span>
        <div id="slug">{{ $post['slug'] }}</div>
    </div>
@endsection

@section('panel-col-3')
    <div class="cell small-24">
        <span>Created</span>
        <div id="created_at">{{ $post->created_at->format('F d Y, h:ia') }}</div>
        <span>Updated</span>
        <div id="updated_at">{{ $post->updated_at->format('F d Y, h:ia') }}</div>
    </div>
@endsection

@section('panel-col-1')
    <div class="cell small-24">
        <div class="component-button">
            <a href="{{ route('posts.edit', $post->id) }}" class="edit">
                <span>Edit</span>
            </a>
        </div>
        <div class="component-button">
            <a href="{{ route('posts.destroy', $post -> id) }}" class="button delete">
                <span>Delete</span>
            </a>
        </div>
    </div>
@endsection
{{--    End admin Panels    --}}

@section('scripts')
    <script>
        const commentsDiv = $('#comment-form, .start-conversation');

        // here, we're searching for a button that has the class "start-conversation"
        $("button.start-conversation").click(function () {

            // check to see whether we've already added the 'has-comments' class
            if (commentsDiv.hasClass('has-comments')) {
                // if so, remove the 'has-comments' class
                commentsDiv.removeClass('has-comments');
            }
            // otherwise, add the 'has-comments' class
            else {
                commentsDiv.addClass('has-comments');
            }
        });
    </script>
    <script src="{{ asset('js/vendors/parsely.js') }}"></script>
    <script src="{{ asset('js/scripts/scrollProgress.js') }}"></script>
@endsection
