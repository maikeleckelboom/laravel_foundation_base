@extends('layouts.app')

@section('title', 'Posts | Edit')

@section('classes')
    class="edit"
@endsection

@section('stylesheets')
    {{--    Exception   --}}
    <script src="//cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'link'
        })
    </script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/parsely.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/select2.css') }}">
@endsection


@section('content')
<div class="grid-x grid-margin-x grid-margin-y">

    <div class="cell small-24">
        <div class="grid-x grid-block">
            <!-- Top left -->
            <div class="cell top-left-22">
                <div class="breadcrumbs-bar">
                    <a href="{{ url()->previous() }}" class="go-back">
                        <img src="{{ asset('files/line-angle-left.svg') }}">
                        <span>{{ __('Back') }}</span>
                    </a>
                    {{ Breadcrumbs::render('blog_single', $post) }}
                </div>
            </div>
            <!-- Top right -->
            <div class="cell top-right-2">

            </div>
        </div>
    </div>


    <div class="cell small-24 medium-18">
        <form method="POST" action="{{ route('posts.update', $post->id) }}" enctype="multipart/form-data" data-parsley-validate>
            <input type="hidden" name="_method" value="PUT">

            <div class="form-group">
                <label name="title">Title:</label>
                <input type="text" id="title" name="title" class="form-control" required value="{{ $post->title }}">
            </div>

            <div class="form-group">
                <label name="slug">Slug:</label>
                <input type="text" name="slug" minlength="5" maxlength="255" required value="{{ $post->slug }}">
            </div>

            <div class="form-group">
                <label name="category_id">Category:</label>
                <select id="category_id" name="category_id">
                    @foreach ($categories as $category)
                        <option {{ ($category->id == $post->category_id) ? 'selected' : '' }} value="{{ $category->id }}">
                            {{ $category->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label name="tag_id">Tags:</label>
                <select name="tags[]" class="select2-multi" multiple="multiple">
                    @foreach($tags as $tag)
                        <option value="{{ $tag['id'] }}" >{{ $tag['name'] }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="featured_image">Update Featured Image</label>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="file" id="featured_image" name="featured_image">
            </div>

            <div class="form-group">
                <label name="body">Body:</label>
                <textarea id="body" name="body" rows="10" required>{{ $post->body }}</textarea>
            </div>

            <div class="component-button">
                <input type="submit" value="Save changes" class="save">
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </div>
        </form>
    </div>

    <div class="cell small-24 medium-6">

        <dl class="dl-horizontal">
            <dt>Category:</dt>
            <dd><span class="category-shape">{{ $post->category['name'] }}</span></dd>
        </dl>


{{--        <dl class="dl-horizontal">--}}
{{--            <dt>Tags:</dt>--}}
{{--            <dd><span class="tag-shape"></span></dd>--}}
{{--        </dl>--}}

        <dl class="dl-horizontal">
            <dt>Created At:</dt>
            <dd>{{ $post->created_at->format('F d, Y') }}</dd>
        </dl>

        <dl class="dl-horizontal">
            <dt>Last Updated:</dt>
            <dd>{{$post->updated_at->format('F d, h:ia')}}</dd>
        </dl>

        <div class="component-button">
            <a href="{{ route('posts.destroy', $post->id) }}" class="button delete">Cancel</a>
        </div>
    </div>

</div>
@endsection


@section('scripts')
    <script src="{{ asset('js/vendors/parsely.js') }}"></script>
    <script src="{{ asset('js/vendors/select2.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2-multi').select2().val({!! json_encode($post->tags()->allRelatedIds()) !!}).trigger('change');
        });
    </script>
@endsection
