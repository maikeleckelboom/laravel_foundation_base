@extends('layouts.app')

@section('title', 'Create')

@section('stylesheets')
    {{--    JS Exception   --}}
    <script src="https://cdn.tiny.cloud/1/uw677gk2zbccsroh2c26ut5na84yf75kge04kxeluvybpk3g/tinymce/5/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'link, image, imagetools'
        })
    </script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/parsely.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/select2.css') }}">
@endsection

@section('classes')
    class="create"
@endsection


@section('content')
    <div class="grid-x">

        <div class="cell small-24">
            <div class="grid-x grid-block">
                <!-- Top left -->
                <div class="cell top-left-22">
                    <div class="breadcrumbs-bar">
                        <a href="{{ url()->previous() }}" class="go-back">
                            <img src="{{ asset('files/line-angle-left.svg') }}">
                            <span>{{__('Back')}}</span>
                        </a>
                        {{ Breadcrumbs::render('create') }}
                    </div>
                </div>
                <!-- Top right -->
                <div class="cell top-right-2">

                </div>
            </div>
        </div>


        <div class="cell small-24">

            <form method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data" data-parsley-validate>

                <div class="grid-x grid-margin-x grid-margin-y">

                    <div class="cell small-12">
                        <div class="form-group">
                            <label name="title">Title</label>
                            <input type="text" id="title" name="title" required>
                        </div>
                    </div>

                    <div class="cell small-12">
                        <div class="form-group">
                            <label name="slug">Slug</label>
                            <input type="text" name="slug" id="slug" minlength="5" maxlength="255" placeholder="Auto generated Slug over here">
                        </div>
                    </div>

                    <div class="cell small-12">
                        <div class="form-group">
                            <label name="category_id">Category</label>
                            <select name="category_id" class="select2-multi category">
                                @foreach($categories as $category)
                                    <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="cell small-12">
                        <div class="form-group">
                            <label name="tag_id">Tags</label>
                            <select name="tags[]" class="select2-multi tags" multiple="multiple"></select>
                        </div>
                    </div>

                    <div class="cell small-24">
                        <div class="form-group">
                            <label for="featured_image">Upload Featured Image</label>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="file" id="featured_image" name="featured_image">
                        </div>
                    </div>

                    <div class="cell small-24">
                        <textarea name="body" rows="10" class="textarea" required></textarea>
                    </div>


                    <div class="cell small-24 component-button">
                        <input type="submit" value="Create Post" class="button create">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                    </div>

                </div>

            </form>

        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('js/vendors/parsely.js') }}"></script>
    <script src="{{ asset('js/vendors/select2.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.select2-multi.tags').select2({
                placeholder: "Choose tags...",
                minimumInputLength: 2,
                ajax: {
                    url: '/tags/find',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });

            $('.select2-multi.category').select2({
                placeholder: "Choose category...",
            });

            $('#title').change(function (e) {
                $.get('{{ route('pages.check_slug') }}',
                    {'title': $(this).val()},
                    function (data) {
                        $('#slug').val(data.slug);
                    }
                );
            });
        });
    </script>
@endsection
