@extends('layouts.app')

@section('title', 'Show')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/select2.min.css') }}">
@endsection

@section('classes')
    class="show"
@endsection

@section('content')
    <div class="grid-x grid-margin-x">

        <div class="cell small-24">
            <div class="grid-x grid-block">
                <!-- Top left -->
                <div class="cell top-left-22">
                    <div class="breadcrumbs-bar">
                        <a href="{{ url()->previous() }}" class="go-back">
                            <img src="{{ asset('files/line-angle-left.svg') }}">
                            <span>{{ __('Back') }}</span>
                        </a>
                        {{ Breadcrumbs::render('blog_single', $post) }}
                    </div>
                </div>
                <!-- Top right -->
                <div class="cell top-right-2">

                </div>
            </div>
        </div>


        <div class="cell small-18">
            <div class="featured-image">
                <img src="{{ asset('storage/blog/images') . '/' . $post->image }}">
            </div>
            <h1>{{ $post->title }}</h1>
            <p class="lead">{!! $post->body !!}</p>
        </div>


        <div class="cell small-6">

            <dl class="dl-horizontal">
                <dt>Url:</dt>
                <dd><a href="{{ route('blog.single', $post->slug) }}">{{ route('blog.single', $post->slug) }}</a></dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Category: </dt>
                <dd>{{ $post->category['name']  }}</dd>
            </dl>

            <dl class="dl-horizontal tags">
                <dt>Tags:</dt>
                @foreach ($post->tags as $tag)
                    <a href="{{ route('tags.show', $tag->id) }}"><span class="tag-shape">{{ $tag->name }}</span></a>
                @endforeach
            </dl>

            <dl class="dl-horizontal">
                <dt>Created:</dt>
                <dd>{{ $post->created_at->format('F d, Y') }}</dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Updated:</dt>
                <dd>{{$post->updated_at->format('F d, h:ia')}}</dd>
            </dl>

            <div class="component-button">
                <a href="{{ route('posts.edit', $post->id) }}" class="edit">Edit</a>
            </div>

            <form action="{{ route('posts.destroy', $post->id) }}" method="post">
                <div class="component-button">
                    <input type="submit" value="Delete" class="delete">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{method_field('DELETE')}}
                </div>
            </form>

        </div>

        <div class="cell small-24" id="backend-comments">
            <h3>Comments <small>{{ $post->comments()->count() }}</small></h3>
            <table class="stacked unstriped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Comment</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($post->comments as $comment)
                    <tr>
                        <td>{{ $comment->name }}</td>
                        <td>{{ $comment->email }}</td>
                        <td>{!!  $comment->comment !!}</td>
                        <td>
                            <a href="{{ route('comments.edit', $comment -> id) }}" class="button edit">
                                <span>Edit</span>
                            </a>
                            <a href="{{ route('comments.delete', $comment -> id) }}" class="button delete">
                                <span>Delete</span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/vendors/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2-multi').select2();
        });
    </script>
@endsection
