@extends('layouts.app')

@section('title', 'Posts | Index')

@section('classes')
    class="posts"
@endsection


@section('content')
    <div class="grid-x grid-margin-y">


        <div class="cell small-24">
            <div class="grid-x grid-block">
                <!-- Top left -->
                <div class="cell top-left-22">
                    <div class="breadcrumbs-bar">
                        <a href="{{ url()->previous() }}" class="go-back">
                            <img src="{{ asset('files/line-angle-left.svg') }}">
                            <span>{{__('Back')}}</span>
                        </a>
                        {{ Breadcrumbs::render('create') }}
                    </div>
                </div>
                <!-- Top right -->
                <div class="cell top-right-2">

                </div>
            </div>
        </div>


        <div class="cell small-24 section">

            <div class="section text-right">
                {!! $posts->links('pagination::foundation-6'); !!}
            </div>

            <table class="unstriped stacked data-table">
                <thead>
                    <th>#</th>
                    <td>User</td>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Body</th>
                    <th>Category</th>
                    <th>Tags</th>
                    <th>Slug</th>
                    <th>Created</th>
                    <th>Updated</th>
                </thead>
                <tbody>
                @foreach ($posts as $post)
                    <tr>
                        <th>{{ $post->id }}</th>
                        <td class="td-user">
                            <img src="/storage/users/avatars/{{ $post->user['avatar'] }}" class="post-user-avatar">
                            <span>{{ $post->user['name'] }}</span>
                        </td>
                        <td>
                            @if( !empty($post->image))
                                <img src="{{ asset('storage/blog/images') . '/' . $post->image }}">
                            @else
                                <img src="{{ asset('storage/blog/images') . '/' . 'default.jpg' }}">
                            @endif
                        </td>
                        <td>{{ $post->title }}</td>
                        <td>{{ substr(strip_tags($post->body), 0, 78) }}{{ strlen(strip_tags($post->body)) > 50 ? "..." : "" }}</td>
                        <td>{{ $post->category['name'] }}</td>
                        <td>
                            @foreach($post->tags as $tag)
                                <a href="{{ route('tags.show', $tag->id) }}">
                                    <span class="tag-shape">{{ $tag->name }}</span>
                                </a>
                            @endforeach
                        </td>
                        <td>{{ $post->slug }}</td>
                        <td>{{ $post->created_at->format('F d, Y') }}</td>
                        <td>{{ $post->updated_at->format('F d, h:ia') }}</td>
                        <td>
                            <div class="component-button">
                                <a href="{{ route('posts.show', $post->id) }}" class="show">View</a>
                            </div>
                            <div class="component-button">
                                <a href="{{ route('posts.edit', $post->id) }}" class="edit">Edit</a>
                            </div>
                            <form action="{{ route('posts.destroy', $post->id) }}" method="post">
                                <div class="component-button">
                                    <input type="submit" class="delete" value="Delete">
                                </div>
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                {{method_field('DELETE')}}
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="section text-right">
                {!! $posts->links('pagination::foundation-6'); !!}
            </div>

        </div>

    </div>
@endsection
