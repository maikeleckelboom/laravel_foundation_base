@extends('layouts.app')

@section('title', 'Categories')

@section('classes')
    class="categories"
@endsection

@section('content')
    <div class="grid-x grid-margin-x grid-margin-y">

        <div class="cell small-24">
            <div class="grid-x grid-block">
                <!-- Top left -->
                <div class="cell top-left">
                    <div class="breadcrumbs-bar">
                        {{ Breadcrumbs::render('categories') }}
                    </div>
                </div>
                <!-- Top right -->
                <div class="cell top-right">

                </div>
            </div>
        </div>

        <div class="cell small-24">
            <div class="back-bar">
                <a href="{{ url()->previous() }}" class="go-back">
                    <img src="{{ asset('files/line-angle-left.svg') }}">
                    {{ __('Back') }}
                </a>
            </div>
        </div>

        <div class="cell small-18">

            <table class="stacked unstriped">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Assoc. posts</th>
                </thead>
                <tbody>
                @foreach ($categories as $category)
                   <tr>
                       <td>{{ $category->id }}</td>
                       <td><a href="{{ route('categories.show', $category->id) }}">{{ $category->name }}</a></td>
                       <td>{{ $category->posts()->count() }}</td>
                   </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <div class="cell small-6">

            <form method="POST" action="{{ route('categories.store') }}" data-parsley-validate>
                <div class="form-group">
                    <label name="name">New Category</label>
                    <input id="category" name="name" class="category" required>
                </div>

                <div class="component-button">
                    <button type="submit" class="button">Create new category</button>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </div>
            </form>
        </div>

    </div>
@endsection
