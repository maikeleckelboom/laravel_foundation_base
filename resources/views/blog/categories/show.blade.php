@extends('layouts.app')

@section('title', "Tags")

@section('classes')

@endsection

@section('content')
    <div class="grid-x">

        <div class="cell small-24">
            <div class="grid-x grid-block">
                <!-- Top left -->
                <div class="cell top-left">
                    <div class="breadcrumbs-bar">
                        {{ Breadcrumbs::render('categories') }}
                    </div>
                </div>
                <!-- Top right -->
                <div class="cell top-right">

                </div>
            </div>
        </div>

        <div class="cell small-24">
            <div class="back-bar">
                <a href="{{ url()->previous() }}" class="go-back">
                    <img src="{{ asset('files/line-angle-left.svg') }}">
                    {{ __('Back') }}
                </a>
            </div>
        </div>

        <div class="cell small-18">
            <h1>{{ $category->name }}
                <small>{{ $category->posts()->count() }} Posts</small>
            </h1>
        </div>

        <div class="cell small-6 action-cell">
            <div class="component-button">
                <a href="{{ route('categories.edit', $category->id) }}" class="button">Edit</a>
            </div>
            <div class="component-button">
                <form action="{{ route('categories.destroy', $category->id) }}" method="post">
                    <input type="submit" class="button delete" value="Delete">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{method_field('DELETE')}}
                </form>
            </div>
        </div>

        <div class="cell small-24">
            <table class="stacked unstriped">
                <thead>
                <th>#</th>
                <th>Title</th>
                <th>Categories</th>
                <th></th>
                </thead>
                <tbody>
                @foreach ($category->posts as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->title }}</td>
                        <td>
                            @foreach($category->tags as $category)
                                <a href="{{ route('categories.show', $category->id) }}"><span class="category-shape">{{ $category->name }}</span></a>
                            @endforeach
                        </td>
                        <td>
                            <div class="component-button">
                                <a href="{{ route('posts.show', $category->id) }}" class="button">View</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection
