@extends('layouts.app')

@section('title', 'Comment | Edit')

@section('classes')
    class="delete-comment"
@endsection

@section('stylesheets')
@endsection


@section('content')
    <div class="grid-x grid-margin-x grid-margin-y">

        <div class="cell small-24 medium-12">

            <h1>Delete this comment?</h1>
            <p>
                <strong>Name:</strong> {{ $comment->name }}
                <strong>Email:</strong> {{ $comment->email }}
                <strong>Comment:</strong> {{ $comment->comment }}
            </p>

            <form action="{{ route('comments.destroy', $comment->id) }}" method="POST" data-parsley-validate>
                <div class="component-button">
                    <input type="submit" class="delete" value="Delete">
                </div>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
                {{method_field('DELETE')}}
            </form>

        </div>

    </div>
@endsection


@section('scripts')
@endsection