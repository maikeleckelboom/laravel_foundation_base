@extends('layouts.app')

@section('title', 'Comment | Edit')

@section('classes')
    class="edit-comment"
@endsection

@section('stylesheets')
@endsection


@section('content')
    <div class="grid-x grid-margin-x grid-margin-y">

        <div class="cell small-24 medium-12">
            <form method="POST" action="{{ route('comments.update', $comment->id) }}" data-parsley-validate>
                <input type="hidden" name="_method" value="PUT">

                <div class="form-group">
                    <label name="name">Name:</label>
                    <input type="text" id="name" name="name"  value="{{ $comment->name }}" required disabled>
                </div>
                <div class="form-group">
                    <label name="email">Email:</label>
                    <input type="email" id="email" name="email" value="{{ $comment->email }}" required disabled>
                </div>
                <div class="form-group">
                    <label name="comment">Body:</label>
                    <textarea id="comment" name="comment" rows="10" required>{{ $comment->comment }}</textarea>
                </div>

                <div class="component-button">
                    <input type="submit" value="Save changes" class="save">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </div>
            </form>
        </div>

    </div>
@endsection


@section('scripts')
@endsection