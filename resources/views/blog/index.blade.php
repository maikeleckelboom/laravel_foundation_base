@extends('layouts.app')

@section('title', 'blog | index')

@section('classes')
    class="blog-index"
@endsection


@section('content')
    <div class="grid-x grid-margin-x grid-margin-y">

        <div class="cell small-24">
            <div class="grid-x grid-block">
                <!-- Top left -->
                <div class="cell top-left-22">
                    <div class="breadcrumbs-bar">
                        {{ Breadcrumbs::render('blog') }}
                    </div>
                </div>
                <!-- Top right -->
                <div class="cell top-right-2">
                    <div class="component-button">
                        <button type="button" class="button admin-panel" data-toggle="offCanvasRight">
                            {{__('Admin Panel') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>


        <div class="cell small-24 section">
            @foreach($categories as $category)
                <a href="{{ route('categories.show', $category->id) }}">
                    <span class="category-shape">{{ $category->name }}</span>
                </a>
            @endforeach
        </div>


        <div class="cell small-24 section">
            @foreach($tags as $tag)
                <a href="{{ route('tags.show', $tag->id) }}">
                    <span class="tag-shape">
                        <span class="tag-after">{{ $tag->name }}</span>
                    </span>
                </a>
            @endforeach
        </div>

        @foreach($posts as $post)
        <div class="cell small-24 medium-12 large-8 module">
            <div class="item">

                <div class="blog-featured-image">
                    @if( !empty($post->image))
                        <img src="{{ asset('storage/blog/images') . '/' . $post->image }}">
                    @else
                        <img src="{{ asset('storage/blog/images') . '/' . 'default.jpg' }}">
                    @endif
                </div>

                <div class="blog-title">
                    <span class="heading h4">{{ $post->title }}</span>
                </div>

                <div class="blog-published">
                    <p>{{ $post->created_at->format('F d, Y') }}</p>
                </div>

                <div class="blog-category">
                    <span class="">{{ $post->category['name'] }}</span>
                </div>

                <div class="blog-body">
                    <p>{{ substr(strip_tags($post->body), 0, 200) }}{{ strlen(strip_tags($post->body)) > 50 ? "..." : "" }}</p>
                </div>

                <div class="blog-cta">
                    <a href="{{ url('blog/'.$post->slug) }}" class="read-more-button">
                        @include('partials.svg.angle-right')
                        {{__('Lees meer') }}
                    </a>
                </div>

            </div>
        </div>
        @endforeach

        <div class="text-center">
            {!! $posts->links(); !!}
        </div>


    </div>
@endsection

@section('scripts')
    <script src="/js/scripts/come-in-module.js"></script>
@endsection
