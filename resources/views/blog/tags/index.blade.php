@extends('layouts.app')

@section('title', 'Blog | Tags')

@section('classes')
    class="tags-index"
@endsection

@section('content')
    <div class="grid-x grid-margin-x grid-margin-y">

        <div class="cell small-24">
            <div class="grid-x grid-block">
                <!-- Top left -->
                <div class="cell top-left">
                    <div class="breadcrumbs-bar">
                        {{ Breadcrumbs::render('tags') }}
                    </div>
                </div>
                <!-- Top right -->
                <div class="cell top-right">

                </div>
            </div>
        </div>

        <div class="cell small-24">
            <div class="back-bar">
                <a href="{{ url()->previous() }}" class="go-back">
                    <img src="{{ asset('files/line-angle-left.svg') }}">
                    {{ __('Back') }}
                </a>
            </div>
        </div>


        <div class="cell small-18">

            <table class="stacked unstriped">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Assoc. posts</th>
                </thead>
                <tbody>
                @foreach ($tags as $tag)
                    <tr>
                        <td>{{ $tag->id }}</td>
                        <td><a href="{{ route('tags.show', $tag->id) }}"><span class="tag-shape">{{ $tag->name }}</span></a></td>
                        <td>{{ $tag->posts()->count() }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <div class="cell small-6">

            <form method="POST" action="{{ route('tags.store') }}" data-parsley-validate>
                <div class="form-group">
                    <label name="nane">New Tag</label>
                    <input id="tag" name="name" class="tag" required>
                </div>

                <div class="component-button">
                    <button type="submit" class="button">Create new tag</button>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </div>
            </form>
        </div>

    </div>
@endsection
