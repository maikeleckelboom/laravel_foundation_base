@extends('layouts.app')

@section('title', "Tags")

@section('classes')
    class="show-single-tag"
@endsection


@section('content')
    <div class="grid-x">

        <div class="cell small-24">
            <div class="grid-x grid-block">
                <!-- Top left -->
                <div class="cell top-left">
                    <div class="breadcrumbs-bar">
                        {{ Breadcrumbs::render('tags') }}
                    </div>
                </div>
                <!-- Top right -->
                <div class="cell top-right">

                </div>
            </div>
        </div>

        <div class="cell small-24">
            <div class="back-bar">
                <a href="{{ url()->previous() }}" class="go-back">
                    <img src="{{ asset('files/line-angle-left.svg') }}">
                    {{ __('Back') }}
                </a>
            </div>
        </div>


        <div class="cell small-18">
            <h1>{{ $tag->name }}
                <small>{{ $tag->posts()->count() }} Posts</small>
            </h1>
        </div>

        <div class="cell small-6 action-cell">
            <div class="component-button">
                <a href="{{ route('tags.edit', $tag->id) }}" class="button">Edit</a>
            </div>
            <div class="component-button">
                <form action="{{ route('tags.destroy', $tag->id) }}" method="post">
                    <input type="submit" class="button delete" value="Delete">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{method_field('DELETE')}}
                </form>
            </div>
        </div>

        <div class="cell small-24">
            <table class="stacked unstriped">
                <thead>
                <th>#</th>
                <th>Title</th>
                <th>Tags</th>
                <th>Action</th>
                </thead>
                <tbody>
                @foreach ($tag->posts as $post)
                    <tr>
                        <td>{{ $post->id }}</td>
                        <td>{{ $post->title }}</td>
                        <td>
                            @foreach($post->tags as $tag)
                                <a href="{{ route('tags.show', $tag->id) }}"><span class="tag-shape">{{ $tag->name }}</span></a>
                            @endforeach
                        </td>
                        <td>
                            <div class="component-button">
                                <a href="{{ route('posts.show', $post->id) }}" class="button">View</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection
