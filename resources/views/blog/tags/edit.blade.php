@extends('layouts.app')

@section('title', 'Edit Tags')

@section('classes')
    class="tags-edit"
@endsection

@section('content')
    <div class="grid-x">

        <div class="cell small-24">
            <div class="grid-x grid-block">
                <!-- Top left -->
                <div class="cell top-left">
                    <div class="breadcrumbs-bar">
                        {{ Breadcrumbs::render('tags') }}
                    </div>
                </div>
                <!-- Top right -->
                <div class="cell top-right">

                </div>
            </div>
        </div>

        <div class="cell small-24">
            <div class="back-bar">
                <a href="{{ url()->previous() }}" class="go-back">
                    <img src="{{ asset('files/line-angle-left.svg') }}">
                    {{ __('Back') }}
                </a>
            </div>
        </div>


        <div class="cell small-12">
            <form action="{{ route('tags.update', $tag->id) }}" method="POST" data-parsley-validate>
                <input type="hidden" name="_method" value="PUT">
                <label>Title:</label>

                <input name="name" type="text" value="{{ $tag->name }}" required>

                <div class="component-button">
                    <input type="submit" class="button" value="Save changes">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </div>
            </form>
        </div>
    </div>
@endsection
