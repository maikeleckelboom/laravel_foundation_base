<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('partials.head')
</head>
<body @yield('classes')>

@include('partials.header')
@include('partials.offcanvas')
@include('partials.admin.panel')

<div class="off-canvas-content" data-off-canvas-content>
    <div class="grid-container">

        @include('partials.messages')
        @yield('content')
    </div>
</div>

<div class="off-canvas-content">
    @include('partials.footer')
</div>

@include('cookieConsent::index')

@include('partials.javascript')
</body>
</html>
