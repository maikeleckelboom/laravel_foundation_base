@extends('layouts.app')

@section('title', 'Home')

@section('classes')
    class="profile"
@endsection

@section('content')
    <div class="grid-x">


        <div class="cell small-24">
            <div class="grid-x grid-margin-x user-credits" >
                <div class="cell small-24 medium-12">
                    <img src="/storage/users/avatars/{{ Auth::user()->avatar }}" class="user-avatar">
                </div>


                <div class="cell small-24 medium-13">
                    <form enctype="multipart/form-data" action="{{ route('profile.update') }}" method="POST">
                        <label>Update Profile Image</label>
                        <input type="file" name="avatar">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="button save"><i class="fas fa-save"></i> Save</button>
                    </form>
                </div>

                <div class="cell small-24 medium-12">

                    <label>Name:</label>
                    <p class="lead">{{ Auth::user()->name }}</p>

                    <label>Email:</label>
                    <p class="lead">{{ Auth::user()->email }}</p>

                    <label>Member since:</label>
                    <p class="lead">{{ Auth::user()->created_at->format('F d, Y') }}</p>


                </div>
            </div>
        </div>


    </div>
@endsection
