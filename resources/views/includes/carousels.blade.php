@extends('layouts.app')

@section('users')
    <div class="grid-x">
        <div class="cell small-24">
            <h4>Gebruikers</h4>

            <div class="owl-carousel users-carousel">

                <div class="item">
                    <div class="user-container">
                        <img src="files/users/user1.png">
                        <div class="user-options">
                            <div class="grid-y">


                                <div class="cell small-6 user-select-option">
                                    <a href="#">
                                        <i class="far fa-user"></i>
                                    </a>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('posts')
    <div class="grid-x">
        <div class="cell small-24">
            <h4>Berichten</h4>
            <div class="owl-carousel">
                <div class="item"><img src="http://placehold.it/150x150"></div>
            </div>
        </div>
    </div>
@endsection