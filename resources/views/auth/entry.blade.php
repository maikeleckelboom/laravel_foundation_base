<button class="close-button" data-close>&times;</button>

<ul class="tabs" data-active-collapse="true" data-tabs id="collapsing-tabs">
    <li class="tabs-title is-active">
        <a href="#panel-login" aria-selected="true">{{ __('Login') }}</a>
    </li>

    <li class="tabs-title">
        <a href="#panel-register">{{ __('Register') }}</a>
    </li>
</ul>

<div class="tabs-content" data-tabs-content="collapsing-tabs">

    <div class="tabs-panel is-active" id="panel-login">
        <div class="molecule-entry">
            <form class="" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="">{{ __('Email adress') }}</label>

                    <input id="email" type="email" class="" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="">{{ __('Password') }}</label>

                    <div class="">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="">
                    <div class="">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="">
                    <button type="submit" class="button">
                        {{ __('Login') }}
                    </button>
                    <a class="link float-right" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                </div>
            </form>
        </div>
    </div>

    <div class="tabs-panel" id="panel-register">
        <div class="molecule-entry">
            <form method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Name</label>

                    <div>
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">E-Mail Address</label>
                    <div>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Password</label>
                    <div>
                        <input id="password" type="password" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="">
                    <label for="password-confirm">Confirm Password</label>
                    <div>
                        <input id="password-confirm" type="password" name="password_confirmation" required>
                    </div>
                </div>

                <div class="">
                    <div>
                        <button type="submit" class="button">
                            Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
