<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

{{--<!-- CSRF Token -->--}}
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Jquery -->
<script src="{{ asset('js/jquery/jquery.min.js') }}"></script>

<!-- Fontawesome stylesheets-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
      integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

<!-- Page depending stylesheets-->
@yield('stylesheets')

<!-- Overlay Scrollbars -->
<link type="text/css" href="{{ asset('css/vendors/OverlayScrollbars.css') }}" rel="stylesheet"/>

<!-- App stylesheets-->
<link rel="stylesheet" href="{{ asset('css/app.css') }}" />

<!-- Page title-->
<title>Laravel | @yield('title')</title>
