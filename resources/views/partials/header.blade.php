<progress value="0" max="1"></progress>

<header class="header">
    <div class="header-logo-content">
        <div class="links">
            <a href="/">

            </a>
        </div>
    </div>

    <div class="header-navigation">
        <div class="menu-navigation">
            <div class="grid-container">
                <div class="grid-x">
                    <div class="cell small-18">

                        <form class="search-form" action="">
                            {{ csrf_field() }}
                            <div class="input-group search-group">
                                <input type="text"  name="searchTerm" class="form-control search-control search" id="search" placeholder="Enter your search term...">
                            </div>
                        </form>

                    </div>


                    <div class="cell auto">
                        @auth
                            <a href="#" claoss="profile-button" data-toggle="profile-menu">
                                <img src="/storage/users/avatars/{{ Auth::user()->avatar }}" class="user-avatar">
                                <span class="user-initials">{{ Auth::user()->initials() }}</span>
                            </a>

                            <div class="profile-menu-modal" id="profile-menu" data-toggler=".menu-open">
                                <ul class="no-bullet menu">

                                    <li class="profile-block">
                                        <img src="/storage/users/avatars/{{ Auth::user()->avatar }}" class="user-avatar">
                                        <div class="user-details">
                                            <p class="username">{{ Auth::user()->name }}</p>
                                            <p class="email">{{ Auth::user()->email }}</p>
                                        </div>
                                    </li>

                                    <li class="menu-hover">
                                        <a href="{{ url('profile') }}">
                                            <span>Mijn profiel</span>
                                        </a>
                                    </li>

                                    <li class="menu-hover">
                                        <a href="{{ url('logout') }}" class="menu-logout">
                                            <span>Logout</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        @endauth
                        @guest
                            <a href="#entry" data-open="modal" class="entry-button">
                                <img src="{{ asset('files/icons/streamline/login-2.2.svg') }}">
                            </a>
                        @endguest
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>

<nav class="hamburger-container" data-toggle="offCanvas">
    <div class="hamburger-menu">
        <input type="checkbox">
        <span></span>
        <span></span>
        <span></span>
    </div>
</nav>

<div id="modal" class="reveal" data-reveal
     data-ajax-url="{{ url('entry') }}">
</div>

{{--<div id="profile-menu-modal" class="reveal text-center" data-reveal--}}
{{--     data-overlay="false" data-close-on-click="true"--}}
{{--     data-ajax-url="{{ url('profile-menu') }}">--}}
{{--    <img src="{{ asset('files/Spinner-1s-200px.svg') }}">--}}
{{--</div>--}}
