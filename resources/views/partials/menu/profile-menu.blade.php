<ul class="no-bullet menu show-profile-menu">
    <li class="profile-block">
        <img src="/storage/users/avatars/{{ Auth::user()->avatar }}" class="user-avatar">
        <div class="user-details">
            <p class="username">{{ Auth::user()->name }}</p>
            <p class="email">{{ Auth::user()->email }}</p>
        </div>
    </li>

    <li class="menu-hover">
        <a href="{{ url('create') }}">
            <img src="{{ asset('files/icons/topic-1-icon.png') }}">
            <span>Create new post </span>
        </a>
    </li>


    <li class="menu-hover">
        <a href="{{ url('profile') }}">
            <img src="{{ asset('files/icons/profile-icon.png') }}">
            <span>My profile</span>
        </a>
    </li>


    <li class="menu-hover">
        <a href="{{ url('categories') }}">
            <img src="{{ asset('files/icons/list-icon.png') }}">
            <span>My categories</span>
        </a>
    </li>

    <li class="menu-hover">
        <a href="{{ url('tags') }}">
            <img src="{{ asset('files/icons/tags-2-icon.png') }}">
            <span>My Tags</span>
        </a>
    </li>

    <li class="menu-hover">
        <a href="{{ url('posts') }}">
            <img src="{{ asset('files/icons/topic-2-icon.png') }}">
            <span>My posts</span>
        </a>
    </li>
    <li class="menu-hover">
        <a href="{{ url('logout') }}" class="menu-logout">
            <img src="{{ asset('files/icons/logout-icon.png') }}">
            <span>Logout</span>
        </a>
    </li>
</ul>
