<footer class="footer">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-24">
                <ul class="no-bullet laravel-foundation-info">
                    <li>
                        <a href="https://laravel.com/docs/5.8" target="_blank">Laravel {{ app()->version() }}</a>
                    </li>
                    <li>
                        <a href="https://foundation.zurb.com/sites/docs/" target="_blank">Foundation 6.5.0</a>
                    </li>
                </ul>
                <div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
            </div>
        </div>
    </div>
</footer>
