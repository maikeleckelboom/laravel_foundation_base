@if (Session::has('success'))
    <div class="callout messages-notification success" data-closable>
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>

        <p>{{Session::get('success')}}</p>

    </div>
@endif

@if (count($errors) > 0)
    <div class="callout warning messages-notification error" data-closable>
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>

        <h5>Data insert unsuccessfull</h5>
        <p>Errors:</p>

        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>

    </div>
@endif



{{--@if (Session::has('success'))--}}
{{--    <div class="relative-unit">--}}
{{--        <div class="messages-notification success">--}}
{{--            <span>{{Session::get('success')}}</span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endif--}}


{{--@if (count($errors) > 0)--}}
{{--    <div class="relative-unit">--}}
{{--        <div class="messages-notification error">--}}
{{--            <ul>--}}
{{--                @foreach ($errors->all() as $error)--}}
{{--                    <li>{{ $error }}</li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endif--}}

<script>
    $('.messages-notification')
        .animate({right: '0', 'opacity': '1'})
        .delay(3500)
        .animate({right: '-99em', 'opacity': '0'});
</script>
