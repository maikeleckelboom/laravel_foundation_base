<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/scripts/scrollProgress.js') }}"></script>
<script src="{{ asset('js/vendors/jquery.overlayScrollbars.js') }}"></script>
<script src="{{ asset('js/scripts/layout.js') }}"></script>
@yield('scripts')