<div class="off-canvas position-left reveal-for-medium" id="offCanvas" data-off-canvas>

    <div class="canvas-logo-content">
        <div class="links">
            <a href="/">
{{--                <button class="close-button" aria-label="Close menu" type="button" data-close>--}}
{{--                    <span aria-hidden="true">&times;</span>--}}
{{--                </button>--}}
            </a>
        </div>

    </div>

    <div class="canvas-accordion-content">
        <ul class="vertical menu accordion-menu" data-accordion-menu data-submenu-toggle="true">
            @auth
                <li class="menu-hover">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('files/icons/simple-house-thin.svg') }}">
                        <span>Home</span>
                        @include('partials.svg.angle-right')
                    </a>
                </li>
                <li class="menu-hover with-sub-level">
                    <a href="{{ url('blog') }}">
                        <img src="{{ asset('files/icons/streamline/screen-1.svg') }}">
                        <span>Blog</span>
                        <span class="sub-level">
                            @include('partials.svg.angle-right')
                        </span>
                        <ul class="vertical nested">
                            <li class="sub-lv">
                                <a href="{{ url('create') }}">
                                    <img src="{{ asset('files/icons/feather-pen.svg') }}">
                                    <span>New post</span>
                                    @include('partials.svg.angle-right')
                                </a>
                            </li>
                            <li class="sub-lv">
                                <a href="{{ url('posts') }}">
                                    <img src="{{ asset('files/speech-bubble.svg') }}">
                                    <span>Posts</span>
                                    @include('partials.svg.angle-right')
                                </a>
                            </li>
                            <li class="sub-lv">
                                <a href="{{ url('tags') }}">
                                    <img src="{{ asset('files/icons/streamline/pin.svg') }}">
                                    <span>Tags </span>
                                    @include('partials.svg.angle-right')
                                </a>
                            </li>
                            <li class="sub-lv">
                                <a href="{{ url('categories') }}">
                                    <img src="{{ asset('files/icons/streamline/tablet-touch.svg') }}">
                                    <span>Categories</span>
                                    @include('partials.svg.angle-right')
                                </a>
                            </li>
                        </ul>
                    </a>
                </li>
                <li class="menu-hover">
                    <a href="{{ url('webshop') }}">
                        <img src="{{ asset('files/icons/shopping-cart.svg') }}">
                        <span>Webshop</span>
                        @include('partials.svg.angle-right')
                    </a>
                </li>
            @endauth
            @guest
                <li class="menu-hover">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('files/icons/simple-house-thin.svg') }}">
                        <span>Home</span>
                        @include('partials.svg.angle-right')
                    </a>
                </li>
                <li class="menu-hover">
                    <a href="{{ url('blog') }}">
                        <img src="{{ asset('files/icons/streamline/screen-1.svg') }}">
                        <span>Blog</span>
                        @include('partials.svg.angle-right')
                    </a>
                </li>
            @endguest
        </ul>
    </div>

</div>
