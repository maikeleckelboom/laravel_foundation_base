<div class="off-canvas position-right"
     id="offCanvasRight"
     data-off-canvas
     data-transition="overlap"
     data-content-overlay="false">

    <!-- Your menu or Off-canvas content goes here -->

    <div class="panel-close">
        <button class="close-button" aria-label="Close menu" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="admin-panel-content">

        @yield('panel-col-1')

        @yield('panel-col-2')

        @yield('panel-col-3')

        @yield('panel-col-4')

        @yield('panel-col-5')

        @yield('panel-col-6')

        @yield('panel-col-7')

        @yield('panel-col-8')

        @yield('panel-col-9')

        @yield('panel-col-10')

    </div>

</div>
